== Работа с объектом ConfigMap/Secret

. Создайте `ConfigMap` c именем `app-config` c ключем `MY_NAME` и значением вашей фамилии латиницей (MY_NAME="Alex NP") используя императивную команду
. Посмотрите список configmap и найдите созданный вами.
. Удалите созданный configmap `app-config`

. Создайте `Secret` c именем `app-secret-config` c ключем `MY_SECRET_NAME` и значением вашего nickname в играх латиницей (MY_NAME="Ruba") используя императивную команду
. Посмотрите список secret и найдите созданный вами.
. Удалите созданный secret `app-secret-config`


. Создайте `ConfigMap` c именем `app-config` c ключем `MY_NAME` и значением вашей фамилии латиницей (MY_NAME="Alex NP") используя декларативный подход (`configmap.yaml` в этой директории)
. Создайте `Secret` c именем `app-secret-config` c ключем `MY_SECRET_NAME` и значением вашего nickname в играх латиницей (MY_NAME="Ruba") используя декларативный подход (`secret.yaml` в этой директории)
. добавьте в манифест `pod.yaml` из этого каталога определение перменных окружения:
.. Переменная `MY_NAME` со значением из `ConfigMap` c именем `app-config` и ключем `MY_NAME`
.. Переменная `MY_SECRET_NAME` со значением из `Secret` c именем `app-config` и ключем `MY_SECRET_NAME`
. Запустите созданный под
. Создайте сервис, чтобы убедиться, что ваша конфигурация применилась
+
.Создать сервис NodePort чтобы достучаться до Pod
[source, bash]
----
kubectl expose pod nginx-hello --type=NodePort --port=80
----
+
.получить адрес сервера
[source, bash]
----
echo "http://$(minikube ip):$(kubectl get svc nginx-hello -o=jsonpath='{.spec.ports[?(@.port==80)].nodePort}')"
----
. Откройте браузер по адресу полученному в предыдущем пункте
. Удалите созданные ресурсы (pod, configmap, secret)
.. Не забудьте удалить Service, который создался командой `kubectl expose ...`
+
[source, bash]
----
kubectl delete svс nginx-hello
----

https://kubernetes.io/docs/concepts/configuration/configmap/[Документация по ConfigMap.]

https://kubernetes.io/docs/concepts/configuration/secret/[Документация по Secret.]

https://kubernetes.io/docs/reference/kubectl/cheatsheet/[kubectl cheat sheet.]

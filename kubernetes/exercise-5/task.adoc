== Работа с Service


. Создайте объект Deployment из `deployment.yaml` в этой директории
. Просмотрите список Deployment и найдите созданный вами
.. Сделайте deployment доступным используя императивную команду:
.. type=NodePort
.. port=80
. Просмотрите список Service и найдите созданный вами
. Найдите адрес, по которой доступен сервис
+
[source, bash]
----
echo "http://$(minikube ip):$(kubectl get svc nginx-hello-deployment -o=jsonpath='{.spec.ports[?(@.port==80)].nodePort}')"
----
. Откройте браузер по адресу полученному в предыдущем пункте
. Удалите созданный Service (`kubectl delete ...`)
. Создайте yaml манифест объекта Service
.. name : nginx-hello
.. port : 80
.. type : ClusterIP
. Создайте объект на основе манифеста
. Просмотрите список Service и найдите `nginx-hello`
. Просмотрите список переменныx окружения в поде
.. используйте команду:
+
[source, bash]
----
kubectl get pod
NAME                                      READY   STATUS    RESTARTS   AGE
nginx-hello-deployment-578b97df66-tzsx8   1/1     Running   0          9m47s
kubectl exec nginx-hello-deployment-578b97df66-tzsx8 -- env <1>
----
<1> `nginx-hello-deployment-578b97df66-tzsx8` имя пода каждый раз меняется
. Сделайте запрос в сервис из того же namespace
.. используйте контейнер `busybox` и `wget`
+
[source, bash]
----
kubectl run busybox -it --image=busybox:1.31.1-musl -- sh <1>
If you don't see a command prompt, try pressing enter.
/ # wget <dns-address-here> <2>

Connecting to nginx-hello (10.96.97.180:80)
saving to 'index.html'
index.html           100% |***********************************************************************************************************************************************************|   867  0:00:00 ETA
'index.html' saved
/ # cat index.html <3>
...
/ # env  <4>
/ # exit <5>
----
<1> создать под busybox и подключиться к shell
<2> обратиться к контейнеру через dns созданного Service
<3> вывести на экран содержимое index.html
<4> показать список переменных
<5> exit чтобы прервать сессию

. Сделать запрос из другого namespace
.. создайте новый namespace `cnad`
.. используйте `busybox` и `wget` в namespace `cnad`
+
[source, bash]
----
kubectl run busybox -it --image=busybox:1.31.1-musl -n cnad -- sh <1>
If you don't see a command prompt, try pressing enter.
/ # wget <dns-address-here> <2>

Connecting to nginx-hello (10.96.97.180:80)
saving to 'index.html'
index.html           100% |***********************************************************************************************************************************************************|   867  0:00:00 ETA
'index.html' saved
/ # cat index.html <3>
...
/ # env  <4>
/ # exit <5>

Session ended, resume using 'kubectl attach busybox -c busybox -i -t' command when the pod is running
----
<1> создать под busybox и подключиться к shell
<2> обратиться к контейнеру через dns созданного Service
<3> вывести на экран содержимое index.html
<4> показать список переменных
<5> exit чтобы прервать сессию


. Удалите все созданные ресурсы (deployment, service, busybox pod, namespace)

https://kubernetes.io/docs/concepts/services-networking/service/[Документация по Service.]

https://kubernetes.io/docs/reference/kubectl/cheatsheet/[kubectl cheat sheet.]



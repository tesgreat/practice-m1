== Работа с Ingress

. Убедитесь что nginx ingress Controller установлен в `minikube`
+
[source, bash]
----
minikube addons enable ingress
🌟  The 'ingress' addon is enabled
minikube addons list
|-----------------------------|----------|--------------|
|         ADDON NAME          | PROFILE  |    STATUS    |
|-----------------------------|----------|--------------|
| dashboard                   | minikube | enabled ✅   |
| default-storageclass        | minikube | enabled ✅   |
| efk                         | minikube | disabled     |
| freshpod                    | minikube | disabled     |
| gvisor                      | minikube | disabled     |
| helm-tiller                 | minikube | disabled     |
| ingress                     | minikube | enabled ✅   |
| ingress-dns                 | minikube | disabled     |
| istio                       | minikube | disabled     |
| istio-provisioner           | minikube | disabled     |
| logviewer                   | minikube | disabled     |
| metrics-server              | minikube | enabled ✅   |
| nvidia-driver-installer     | minikube | disabled     |
| nvidia-gpu-device-plugin    | minikube | disabled     |
| registry                    | minikube | disabled     |
| registry-aliases            | minikube | disabled     |
| registry-creds              | minikube | disabled     |
| storage-provisioner         | minikube | enabled ✅   |
| storage-provisioner-gluster | minikube | disabled     |
|-----------------------------|----------|--------------|
----

. Создайте ресурсы из файла `nginx-hello.yaml` необходимые для упражнения:
+
.`nginx-hello.yaml`
[source, yaml]
----
include::nginx-hello.yaml[]
----
. Создайте Ingress манифест yaml для Service `nginx-hello`
.. имя name: nginx-hello
.. добавьте аннотацию `nginx.ingress.kubernetes.io/ssl-redirect: "false"`
.. path: /
.. перенаправить трафик от ingress к `nginx-hello` и `port:http`
. Примените Ingress манифест
. Узнайте адрес Ingress в minikube
+
[source, bash]
----
echo "http://$(minikube ip)"
----
. Откройте браузер и убедитесь, что приложение открывается.
. Удалите все созданные ресурсы (deployment, service, ingress)

https://kubernetes.io/docs/concepts/services-networking/ingress/[Документация по Ingress.]

https://kubernetes.io/docs/reference/kubectl/cheatsheet/[kubectl cheat sheet.]


